﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
    using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
    using Otus.Teaching.PromoCodeFactory.Core.Domain;

    public class DomainContext : IDomainContext
    {
        public DomainContext()
        {
            this.Employees = new BaseEntityStorage<Employee>();

            this.Roles = new BaseEntityStorage<Role>();

            this.Employees.AddRange(FakeDataFactory.Employees);

            this.Roles.AddRange(FakeDataFactory.Roles);
        }

        public BaseEntityStorage<Employee> Employees { get; set; }

        public BaseEntityStorage<Role> Roles { get; set; }
    }
}
