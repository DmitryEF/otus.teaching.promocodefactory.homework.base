﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
    using Otus.Teaching.PromoCodeFactory.Core.Domain;

    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private readonly BaseEntityStorage<T> storage;

        public InMemoryRepository(BaseEntityStorage<T> storage)
        {
            this.storage = storage;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return this.storage.GetAllAsync();
        }

        public Task<T> GetByIdAsync(Guid id)
        {
           return this.storage.GetByIdAsync(id);
        }

        public async Task<bool> CreateAsync(T entity)
        {
            if (entity == null) return false;

            var dataCount = await this.storage.GetCountAsync();

            await this.storage.AddAsync(entity);

            return dataCount + 1 == await this.storage.GetCountAsync();
        }

        public async Task<bool> DeleteAsync(T entity)
        {
            if (entity == null) return false;

            if(this.storage.GetByIdAsync(entity.Id) == null)
                return false;
            
            return await this.storage.DeleteAsync(entity);
        }

        public async Task<bool> ExistsAsync(Guid id)
        {
            return await this.storage.ExistsAsync(id);
        }

        public async Task<T> UpdateAsync(T updatesForEntity)
        {
            var existingEntity = await this.GetByIdAsync(updatesForEntity.Id);

            if (existingEntity == null) return null;

            existingEntity = updatesForEntity;

            return existingEntity;
        }
    }
}