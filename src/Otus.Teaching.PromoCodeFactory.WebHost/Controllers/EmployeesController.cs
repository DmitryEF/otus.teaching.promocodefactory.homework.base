﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;
    using Otus.Teaching.PromoCodeFactory.WebHost.Converters;

    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        private readonly IRepository<Role> _roleRepository;

        private IEnumerable<Role> _actualRoles;

        public EmployeesController(IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;

            _roleRepository = roleRepository;

            _actualRoles = _roleRepository.GetAllAsync().Result;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse(employee);            

            return employeeModel;
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <param name="employeeCreateRequest"></param>
        /// <returns></returns>
        [HttpPut()]
        public async Task<ActionResult<Guid?>> CreateAsync(EmployeeCreateRequest employeeCreateRequest)
        {
            if (employeeCreateRequest == null) return BadRequest("Request is null!");

            var employee = ModelToEntityConverter.ConvertToEmployee(employeeCreateRequest, _actualRoles);

            if (employee == null) return BadRequest("Can't convert request data to Employee!");

            Guid? result = employee.Id;

            if (!await _employeeRepository.CreateAsync(employee)) result = null;

            return result;
        }

        /// <summary>
        /// Изменить данные сотрудника
        /// </summary>
        /// <param name="employeeUpdateRequest"></param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<ActionResult<EmployeeResponse>> UpdateAsync(EmployeeUpdateRequest employeeUpdateRequest)
        {
            if (employeeUpdateRequest == null || !await _employeeRepository.ExistsAsync(employeeUpdateRequest.Id))
                return BadRequest("Request is null or employee not exists!");

            var employee = 
                ModelToEntityConverter.ConvertToEmployee(
                    employeeUpdateRequest, 
                    await _employeeRepository.GetByIdAsync(employeeUpdateRequest.Id), 
                    _actualRoles);

            return new EmployeeResponse(await _employeeRepository.UpdateAsync(employee)); ;
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<bool>> DeleteAsync(Guid id)
        {
            if (id == null || !await _employeeRepository.ExistsAsync(id)) 
                return BadRequest("Employee with such id not exists!");

            var employee = await _employeeRepository.GetByIdAsync(id);            

            return await _employeeRepository.DeleteAsync(employee);          
        }
    }
}