﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class EmployeeResponse
    {
        public EmployeeResponse(Employee employee)
        {
            this.Id = employee.Id;
            this.FullName = employee.FullName;
            this.Email = employee.Email;
            this.AppliedPromocodesCount= employee.AppliedPromocodesCount;
            this.Roles = employee.Roles.Select(x => new RoleItemResponse()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description
            }).ToList();
        }

        public Guid Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public List<RoleItemResponse> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}