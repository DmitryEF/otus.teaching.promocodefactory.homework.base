﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    using System;

    public class EmployeeDeleteRequest
    {
        public Guid Id { get; set; }
    }
}
