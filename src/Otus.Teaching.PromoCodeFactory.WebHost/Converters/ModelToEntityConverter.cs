﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Converters
{
    using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    public static class ModelToEntityConverter
    {
        public static Employee ConvertToEmployee(EmployeeUpdateRequest employeeUpdateRequest, Employee employee, IEnumerable<Role> actualRoles)
        {
            if (employeeUpdateRequest.RoleIds != null
               && employeeUpdateRequest.RoleIds.Any())
            {
                employee.Roles = actualRoles
                    .Where(r => employeeUpdateRequest.RoleIds.Contains(r.Id)).ToList();
            }

            if (!string.IsNullOrEmpty(employeeUpdateRequest.FirstName))
            {
                employee.FirstName = employeeUpdateRequest.FirstName;
            }

            if (!string.IsNullOrEmpty(employeeUpdateRequest.LastName))
            {
                employee.LastName = employeeUpdateRequest.LastName;
            }

            if (!string.IsNullOrEmpty(employeeUpdateRequest.Email))
            {
                employee.Email = employeeUpdateRequest.Email;
            }

            if (employeeUpdateRequest.AppliedPromocodesCount != null)
            {
                employee.AppliedPromocodesCount = (int)employeeUpdateRequest.AppliedPromocodesCount;
            }

            return employee;
        }

        public static Employee ConvertToEmployee(EmployeeCreateRequest employeeCreateRequest, IEnumerable<Role> actualRoles)
        {
            if (employeeCreateRequest.RoleIds == null || !employeeCreateRequest.RoleIds.Any())
                return null;

            return new Employee
            {
                FirstName = employeeCreateRequest.FirstName,
                LastName = employeeCreateRequest.LastName,
                Email = employeeCreateRequest.Email,
                Roles = actualRoles.Where(r => employeeCreateRequest.RoleIds.Contains(r.Id)).ToList(),
                AppliedPromocodesCount = employeeCreateRequest.AppliedPromocodesCount
            };
        }
    }
}
