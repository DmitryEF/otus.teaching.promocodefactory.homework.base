﻿namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Otus.Teaching.PromoCodeFactory.Core.Domain;

    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<bool> CreateAsync(T entity);

        Task<bool> DeleteAsync(T entity);

        Task<bool> ExistsAsync(Guid id);

        Task<T> UpdateAsync(T updatesForEntity);
    }
}