﻿namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions
{
    using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
    using Otus.Teaching.PromoCodeFactory.Core.Domain;

    public interface IDomainContext
    {
        BaseEntityStorage<Employee> Employees { get; set; }

        BaseEntityStorage<Role> Roles { get; set; }
    }
}