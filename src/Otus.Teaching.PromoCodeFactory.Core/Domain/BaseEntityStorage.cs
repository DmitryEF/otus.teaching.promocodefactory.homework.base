﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class BaseEntityStorage<T> where T : BaseEntity
    {
        private List<T> storage;

        public BaseEntityStorage() 
        {
            this.storage = new List<T>();
        }

        public void AddRange(IEnumerable<T> entities)
        {
            this.storage.AddRange(entities);
        }

        public Task AddAsync(T entity)
        {
            return Task.Run(() => this.storage.Add(entity));
        }

        public Task<IEnumerable<T>> GetAllAsync() 
        { 
            return Task.FromResult(this.storage as IEnumerable<T>);
        }

        public Task<T> GetByIdAsync(Guid id) 
        {
            return Task.FromResult(this.storage.FirstOrDefault(i => i.Id == id));
        }

        public Task<int> GetCountAsync()
        {
            return Task.FromResult(this.storage.Count);
        }

        public Task<bool> DeleteAsync(T entity)
        {
            return Task.FromResult(this.storage.Remove(entity));
        }

        public Task<bool> ExistsAsync(Guid id)
        {
            return Task.FromResult(this.storage.Any(e => e.Id == id));
        }
    }
}
