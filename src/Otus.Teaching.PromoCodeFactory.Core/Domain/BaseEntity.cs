﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    using System;

    public class BaseEntity
    {
        public BaseEntity()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
    }
}