﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Entities;
    using System.Collections.Generic;

    public class Employee
        : BaseEntity, IEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public List<Role> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}