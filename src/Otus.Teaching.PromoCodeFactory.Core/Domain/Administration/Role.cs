﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Entities;

    public class Role
        : BaseEntity, IEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}